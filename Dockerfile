FROM golang
WORKDIR /app
COPY . .
RUN go build -o parser/main parser/main.go parser/repository.go
CMD ["/app/parser/main"]

EXPOSE 8080