package main

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"

	"strconv"
	"time"
	"unicode"

	"github.com/PuerkitoBio/goquery"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

const maxTries = 15

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

func main() {
	port := ":8080"

	ctrl := NewVacancyController()
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Post("/search", ctrl.Search)
	r.Post("/delete", ctrl.Delete)
	r.Post("/get", ctrl.Get)
	r.Get("/list", ctrl.List)

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("/app/parser/swagger"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

type VacancyController struct {
	Vac VacancyStorage
}

func NewVacancyController() VacancyController {
	return VacancyController{
		Vac: NewVacancyStorage(),
	}
}

func (v *VacancyController) Search(w http.ResponseWriter, r *http.Request) {
	var err error
	var n int
	var query string
	b := make([]byte, 16)
	for err == nil {
		n, err = r.Body.Read(b)
		query = query + string(b[:n])
	}
	query = query[7:]
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	// переменная нашего веб драйвера
	var wd selenium.WebDriver
	// прописываем адрес нашего драйвера
	urlPrefix := selenium.DefaultURLPrefix
	urlPrefix = "http://selen:4444/wd/hub"
	// немного костылей чтобы драйвер не падал
	i := 1
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}
	// после окончания программы завершаем работу драйвера
	defer wd.Quit()

	// сразу обращаемся к странице с поиском вакансии по запросу
	page := 1 // номер страницы
	err = wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
	if err != nil {
		http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
		return
	}
	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
		return
	}
	vacancyCount, err := elem.Text()
	if err != nil {
		http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
		return
	}
	var count string
	for _, v := range vacancyCount {
		if unicode.IsDigit(v) {
			count += string(v)
		}
	}
	vacancy, err := strconv.Atoi(count)
	if err != nil {
		http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
		return
	}
	page--
	links := make([]string, 0, 64)
	for vacancy > 130 {
		page++
		fmt.Println("PAGE:", page)
		var c int
		err = wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
		if err != nil {
			http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
			return
		}
		elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			http.Error(w, "selenium error: "+err.Error(), http.StatusInternalServerError)
			return
		}
		for _, v := range elems {
			link, err := v.GetAttribute("href")
			if err != nil {
				continue
			}
			c++
			links = append(links, link)
		}
		vacancy -= c
		fmt.Println("vacancies: left", vacancy, ", got", len(links))
	}
	ans := make([]Vacancy, 0, 32)
	for i := 0; i < len(links); i++ {
		resp, err := http.Get("https://career.habr.com" + links[i])
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		doc, err := goquery.NewDocumentFromReader(resp.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		resp.Body.Close()
		dd := doc.Find("script[type=\"application/ld+json\"]")
		if dd == nil {
			http.Error(w, "cant find vacancy's JSON", http.StatusInternalServerError)
			return
		}
		ss := dd.First().Text()
		var vac Vacancy
		var jl1 JobLocation
		json.Unmarshal([]byte(ss), &vac)
		err = json.Unmarshal([]byte(ss), &jl1)
		if err != nil {
			var jl2 JobLocation2
			err = json.Unmarshal([]byte(ss), &jl2)
			if err == nil {
				vac.JobLocation = jl2.JobLocation
			}
		} else {
			vac.JobLocation = jl1.JobLocation
		}
		ans = append(ans, vac)
	}
	for _, vv := range ans {
		v.Vac.Create(vv)
	}
	js, err := json.Marshal(ans)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Write(js)
}

func (v *VacancyController) Delete(w http.ResponseWriter, r *http.Request) {
	var err error
	var n int
	var query string
	b := make([]byte, 16)
	for err == nil {
		n, err = r.Body.Read(b)
		query = query + string(b[:n])
	}
	query = query[3:]
	n, err = strconv.Atoi(query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = v.Vac.Delete(n)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}
}

func (v *VacancyController) Get(w http.ResponseWriter, r *http.Request) {
	var err error
	var n int
	var query string
	b := make([]byte, 16)
	for err == nil {
		n, err = r.Body.Read(b)
		query = query + string(b[:n])
	}
	query = query[3:]
	n, err = strconv.Atoi(query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	vac, err := v.Vac.GetByID(n)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	js, err := json.Marshal(vac)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Write(js)
}

func (v *VacancyController) List(w http.ResponseWriter, r *http.Request) {
	ans := v.Vac.GetList()
	js, err := json.Marshal(ans)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Write(js)
}
