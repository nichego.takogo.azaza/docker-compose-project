package swagger

import rep "gitlab.com/nichego.takogo.azaza/docker-compose-project/parser"

//go:generate swagger generate spec -o swagger.json --scan-models

// swagger:route POST /search all searchRequest
// responses:
//   200:searchResponse
//	 500:search500Response

// swagger:parameters searchRequest
type searchRequest struct {
	// Search request
	// in:formData
	// required:true
	Search string `json:"search"`
}

// swagger:response searchResponse
type searchResponse struct {
	// in:body
	Body []rep.Vacancy
}

// swagger:response search500Response
type search500Response struct {
	// in:body
	Body string
}

// swagger:route POST /delete all deleteRequest
// responses:
// 	200:delete2Response
// 	404:delete4Response

// swagger:parameters deleteRequest
type deleteRequest struct {
	// in:formData
	// required:true
	ID string `json:"id"`
}

// swagger:response delete2Response
type delete2Response struct {
}

// swagger:response delete4Response
type delete4Response struct {
	Body string
}

// swagger:route POST /get all getRequest
// responses:
//   200:get2Response
//	 404:get4Response

// swagger:parameters getRequest
type getRequest struct {
	// in:formData
	// required:true
	ID string `json:"id"`
}

// swagger:response get2Response
type get2Response struct {
	Body rep.Vacancy
}

// swagger:response get4Response
type get4Response struct {
	Body string
}

// swagger:route GET /list all listRequest
// responses:
//   200:listResponse

// swagger:parameters listRequest
type listRequest struct {
}

// swagger:response listResponse
type listResponse struct {
	// in:body
	Body []rep.Vacancy
}
